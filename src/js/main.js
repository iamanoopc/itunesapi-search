var searchForm = document.getElementById('searchForm'),
    searchResults = document.getElementById("searchResults"),
    results = document.getElementById("results")

//Search submit event listener
searchForm.addEventListener("submit", function(event) {
    event.preventDefault()
    var searchTerm = document.getElementById('searchInput').value   
    searchResults.innerHTML = ""
    
    //Fetching the data
    fetch(`https://itunes.apple.com/search?term=${searchTerm}`)
    .then (function (data){
        return data.json()
    })
    .then (function(json) {
      
        var data= json.results
        results.innerHTML = (data.length>0) ?"Results...": "Nothing found..."
        
        //Mapping the data to HTML elements
       
        data.map(function(item){
          var artistName = item.artistName,
              albumImage = item.artworkUrl100,
              songName = item.collectionName,
              songPreview = item.previewUrl,
              
              result = `                          
                          <div class="songcard">
                              <img class="" src="${albumImage}" alt="Card image cap">                              
                              <div class="songcard-body">                                 
                                  <h4 class="songcard-title text-muted font-italic">${songName}</h4>
                                  <p class="text-primary" >${artistName}</p>
                                  <audio controls class ="audio">
                                  <source value="" src="${songPreview}" type="audio/mpeg">
                                  <a href="#" class="btn btn-primary">Play</a>
                              </div>
                          </div>                         
                       `
              // Appending each element to results
              searchResults.insertAdjacentHTML("beforeEnd", result)
          
        })//Data Mapping
        
    })
})

