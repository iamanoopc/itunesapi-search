
var gulp = require('gulp');
var stylus = require('gulp-stylus'); 
var pug = require('gulp-pug');
var watch = require('gulp-watch');
var nodemon = require('gulp-nodemon');  
var jshint = require('gulp-jshint');  
var livereload = require('gulp-livereload');



gulp.task('pug',function() {
 return gulp.src('pug/**/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('./src/'))
    .pipe(livereload({ start: true }));;
});


gulp.task('stylus', function () {
  return gulp.src('./stylus/style.styl')
    .pipe(stylus())
    .pipe(gulp.dest('./src/css/'))
    .pipe(livereload({ start: true }));

});


gulp.task('scripts', function() {  
  return gulp.src('src/js/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(livereload({ start: true }));
});

 
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('pug/**/*.pug', ['pug']);
  gulp.watch('stylus/*.styl', ['stylus']);
  gulp.watch('src/js/*.js', ['scripts']);
});

gulp.task('server',function(){  
    nodemon({
        'script': 'server.js'
    });
});



gulp.task('default', [ 'server','watch']);
